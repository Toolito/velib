<?php

return array(
	'registration' => '/user/social_register',
	'login'        => '/user/social_login',
	'callback'     => 'connect/callback',
	
	'registered'   => '/',
	'logged_in'    => '/',
);