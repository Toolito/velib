<?php

return array(
	/**
	 * Providers
	 *
	 * Providers such as Facebook, Twitter, etc all use different Strategies such as oAuth, oAuth2, etc.
	 * oAuth takes a key and a secret, oAuth2 takes a (client) id and a secret, optionally a scope.
	 */
	'providers' => array(

		'basecamp' => array(
			'id'     => '',
			'secret' => '',
		),

		'dropbox' => array(
			'key'    => '',
			'secret' => '',
		),

		'facebook' => array(
			'id'     => '402652853143236',
			'secret' => 'c625b1c56e7ef44414d86add7561ff19',
			'scope'  => 'email',
		),

		'flickr' => array(
			'key'    => '',
			'secret' => '',
		),

		'foursquare' => array(
			'id'     => '',
			'secret' => '',
		),

		'github' => array(
			'id'     => '',
			'secret' => '',
		),

		'google' => array(
			'id'     => '594556625982.apps.googleusercontent.com',
			'secret' => 'G0_masB0Q1HzHTcl8_slVNRC',
		),

		'instagram' => array(
			'id'     => '',
			'secret' => '',
		),

		'linkedin' => array(
			'key'    => '',
			'secret' => '',
		),

		'paypal' => array(
			'id'     => '',
			'secret' => '',
		),

		'soundcloud' => array(
			'id'     => '',
			'secret' => '',
		),

		'tumblr' => array(
			'key'    => '',
			'secret' => '',
		),

		'twitter' => array(
			'key'    => 'lhcm3lQ0aKMCCTw8rWPKoQ',
			'secret' => 'EO7SbCX8j09ZacUQkukUPpI8EcjQIjurCoZMj9DvIuk',
		),

		'vimeo' => array(
			'key'    => '',
			'secret' => '',
		),

		'windowslive' => array(
			'id'     => '',
			'secret' => '',
		),

	),
);