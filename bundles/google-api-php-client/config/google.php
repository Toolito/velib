<?php
/**
 * @author Andrew Zappella < http://www.suisseo.ch/ >
 * @copyright 2012 Suisseo SARL
 * @license http://creativecommons.org/licenses/by-sa/3.0/
 * @package Google API PHP Client (Laravel Bundle)
 * @version 0.1 - 2012-07-31
 */

// Visit https://code.google.com/apis/console to generate your
// client id, client secret, and to register your redirect uri.
return array(
  'application_name' => 'Bike 2.0', 
  'client_id' => '594556625982.apps.googleusercontent.com',
  'client_secret' => 'G0_masB0Q1HzHTcl8_slVNRC',
  'redirect_uri' => 'http://localhost/velo/public/',
  'developer_key' => 'AIzaSyDlThhHY51UBM1KRmtbWiJ2xqg6RJ8FqmI',
  // e.g. for Google Books API
  'set_scopes' => array('https://www.googleapis.com/auth/userinfo.profile'), 
  'access_type' => 'online',
  // Returns objects the Google API Service instead of associative arrays
  'use_objects' => true 
);

