<!DOCTYPE html>
<html lang="en">
<head>
	
	<!-- start: Meta -->
	<meta charset="utf-8">
	<title>Login @ VeliBike</title>
	<meta name="description" content="Perfectum Dashboard Bootstrap Admin Template.">
	<meta name="author" content="Toolito">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="/css/bootstrap.css" rel="stylesheet">
	<link href="/css/bootstrap-responsive.css" rel="stylesheet">
	<link id="base-style" href="/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="/css/style-responsive.css" rel="stylesheet">
	
	<!--[if lt IE 7 ]>
	<link id="ie-style" href="css/style-ie.css" rel="stylesheet">
	<![endif]-->
	<!--[if IE 8 ]>
	<link id="ie-style" href="css/style-ie.css" rel="stylesheet">
	<![endif]-->
	<!--[if IE 9 ]>
	<![endif]-->
	
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- start: Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico">
	<!-- end: Favicon -->
	
		<style type="text/css">
			body { background: url(img/bg-login.jpg) !important; }
		</style>
		
</head>
<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="login-box">
				<?=Form::vertical_open('/user/login', 'POST',array('id'=>'loginform'));?>
					<div class="icons">
						<img src="/img/gly/glyphicons_204_unlock.png"/>
					</div>
					<h2>Login to your account</h2>
					<fieldset>
						<div class="input-prepend" title="Username">
							<span class="add-on"><i class="icon-user"></i></span>
							<input class="input-large span10" name="username" id="username" type="text" placeholder="type username"/>
						</div>
						<div class="input-prepend" title="Password">
							<span class="add-on"><i class="icon-lock"></i></span>
							<input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
						</div>
						<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>
						<div class="button-login">	
							<button type="submit" class="btn btn-primary"><i class="icon-off icon-white"></i> Login</button>
						</div>
					</fieldset>
					<hr>
					<div style="text-align:center;">
						<ul id="social-list">
					  <li class="face"><a href="/connect/session/facebook">&nbsp</a></li>
					  <li class="twit offset2"><a href="/connect/session/twitter">&nbsp</a></li>
					  <li class="goog offset3"><a href="/connect/session/google">&nbsp</a></li>				
						</ul>
					</div>								
					<?=View::make('login.recover')?>
					<?=View::make('login.register')?>
				</form>
	            <?=Form::vertical_open('/home/recover', 'POST',array('id'=>'recoverform'));?>
					<div class="icons">
						<img src="/img/gly/glyphicons_128_message_lock.png"/>
					</div>		            
		            <h2>Récupération de mot de passe</h2>
	                <p>Entrez votre adresse email et vous recevrez les instructions pour redéfinir votre mot de passe.</p>
	                <fieldset>
						<div class="input-prepend" title="Email">
							<span class="add-on"><i class="icon-envelope"></i></span>
							<input class="input-large span10" name="email" id="email" type="text" placeholder="type email"/>
						</div>								                
		                <div class="button-login">
		                    <span class="pull-right"><input type="submit" class="btn btn-inverse" value="Recover" /></span>
		                </div>
		            </fieldset>
					<?=View::make('login.login')?>
	            </form>
                <?=Form::vertical_open('/user/register', 'POST',array('id'=>'registerform'));?>
					<div class="icons">
						<img src="/img/gly/glyphicons_235_pen.png"/>
					</div>
	                <h2>Register an account</h2>
	                <fieldset>
	                    <div class="input-prepend" title="Username">
	                        <span class="add-on"><i class="icon-user"></i></span>
	                        <input class="input-large span10" name="username" id="username" type="text" placeholder="type username"/>
	                    </div>
	                    <div class="input-prepend" title="Email">
	                        <span class="add-on"><i class="icon-envelope"></i></span>
	                        <input class="input-large span10" name="email" id="email" type="text" placeholder="type username"/>
	                    </div>						
	                    <div class="input-prepend" title="Password">
	                        <span class="add-on"><i class="icon-lock"></i></span>
	                        <input class="input-large span10" name="password" id="password" type="password" placeholder="type password"/>
	                    </div>
	                    <div class="input-prepend" title="Confirmer Password">
	                        <span class="add-on"><i class="icon-lock"></i></span>
	                        <input class="input-large span10" name="password_confirmation" id="password_confirmation" type="password" placeholder="confirm password"/>
	                    </div>
	                    <div class="button-login">  
	                        <button type="submit" class="btn btn-primary"><i class="icon-pencil icon-white"></i> Register</button>
	                    </div>
	                </fieldset>
	                <hr>
					<h3><a href="/login"><< Go back</a></h3>   
                </form>	            
	        </div><!--/login box--> 				
		</div><!--/fluid-row-->		
	</div><!--/.fluid-container-->
	<!-- start: JavaScript-->
<script src="js/jquery-1.7.2.min.js"></script>
<script src="js/jquery-ui-1.8.21.custom.min.js"></script>
<script src="js/bootstrap.js"></script>
<script src="js/jquery.cookie.js"></script>
<script src='js/fullcalendar.min.js'></script>
<script src='js/jquery.dataTables.min.js'></script>
<script src="js/excanvas.js"></script>
<script src="js/jquery.flot.min.js"></script>
<script src="js/jquery.flot.pie.min.js"></script>
<script src="js/jquery.flot.stack.js"></script>
<script src="js/jquery.flot.resize.min.js"></script>
<script src="js/jquery.chosen.min.js"></script>
<script src="js/jquery.uniform.min.js"></script>
<script src="js/jquery.cleditor.min.js"></script>
<script src="js/jquery.noty.js"></script>
<script src="js/jquery.elfinder.min.js"></script>
<script src="js/jquery.raty.min.js"></script>
<script src="js/jquery.iphone.toggle.js"></script>
<script src="js/jquery.uploadify-3.1.min.js"></script>
<script src="js/jquery.gritter.min.js"></script>
<script src="js/jquery.imagesloaded.js"></script>	
<script src="js/jquery.masonry.min.js"></script>	
<script src="js/jquery.knob.js"></script>
<script src="js/jquery.sparkline.min.js"></script>
<script src="js/custom.js"></script>
<script src="<?=asset('js/unicorn.js')?>"></script>
	<!-- end: JavaScript-->
	<?=Messages::get_noty();?>
<script>
$(document).ready(function(){
	var login = $('#loginform');
	var recover = $('#recoverform');
	var register = $('#registerform');
	var speed = 400;

	$('#to-recover').click(function(){
		login.fadeTo(speed,0.01).css({'z-index':'100','display':'none'});
		recover.fadeTo(speed,1).css({'z-index':'200'});
		register.fadeTo(speed,0.01).css({'z-index':'100','display':'none'});

	});

	$('#to-login').click(function(){
		recover.fadeTo(speed,0.01).css({'z-index':'100','display':'none'});
		login.fadeTo(speed,1).css({'z-index':'200'});
		register.fadeTo(speed,0.01).css({'z-index':'100','display':'none'});
	});

	$('#to-register').click(function(){
		recover.fadeTo(speed,0.01).css({'z-index':'100','display':'none'});
		login.fadeTo(speed,0.01).css({'z-index':'100','display':'none'});
		register.fadeTo(speed,1).css({'z-index':'200'});
	});	
    
    if($.browser.msie == true && $.browser.version.slice(0,3) < 10) {
        $('input[placeholder]').each(function(){ 
    
        var input = $(this);       
       
        $(input).val(input.attr('placeholder'));
               
        $(input).focus(function(){
             if (input.val() == input.attr('placeholder')) {
                 input.val('');
             }
        });
       
        $(input).blur(function(){
            if (input.val() == '' || input.val() == input.attr('placeholder')) {
                input.val(input.attr('placeholder'));
            }
        });
    	});
    }


	$(function(){
		// Checking for CSS 3D transformation support
		$.support.css3d = supportsCSS3D();

		var formContainer = $('.login-box');

		// Listening for clicks on the ribbon links
		$('.flip-link').click(function(e){

			// Flipping the forms
			formContainer.toggleClass('flipped');

			// If there is no CSS3 3D support, simply
			// hide the login form (exposing the recover one)
			if(!$.support.css3d){
				$('#loginform').toggle();
			}
			e.preventDefault();
		});

		// A helper function that checks for the
		// support of the 3D CSS3 transformations.
		function supportsCSS3D() {
			var props = [
				'perspectiveProperty', 'WebkitPerspective', 'MozPerspective'
			], testDom = document.createElement('a');

			for(var i=0; i<props.length; i++){
				if(props[i] in testDom.style){
					return true;
				}
			}
			return false;
		}
	}); 
});			
</script>
</body>
</html>
