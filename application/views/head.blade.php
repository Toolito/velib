<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>@yield('title') :: Bike 2.0</title>
        <meta name="description" content="" />
        <meta name="author" content="" />

        <!-- start: Mobile Specific -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- end: Mobile Specific -->
        
        <!-- start: CSS -->
        <link id="bootstrap-style" href="/css/bootstrap.css" rel="stylesheet">
        <link href="/css/bootstrap-responsive.css" rel="stylesheet">
        <link id="base-style" href="/css/style.css" rel="stylesheet">
        <link id="base-style-responsive" href="/css/style-responsive.css" rel="stylesheet">

        <!-- for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->
        @yield('begin_script')
    </head>
    <body>