<?=View::make('head')?> <!-- HTML doctype, head et body ouvrante -->
<?=View::make('header')?> <!-- Logo, blue navbar, user infos -->
<div class="container-fluid">
	<div class="row-fluid">
		 <?=View::make('left-menu')?><!-- Menu nav commun à toutes les pages -->
		<div id="content" class="span10" style="min-height: 646px">
	@yield('content')
		</div>
	</div>
	<?=View::make('footer')?>
</div> <!-- /Main container -->
		<?=View::make('inc.scripts')?>
		@yield('scripts')
</body>
</html>