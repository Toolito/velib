<!DOCTYPE HTML>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <title>Google API PHP Client</title>
  <meta name="viewport" content="width=device-width">
</head>
<body>
  <?php if ( Session::has('status') ): ?>
  <p><?php echo Session::get('status'); ?></p>
  <?php endif; ?>

  <?php if ( isset($google_auth_url) ) : ?>
  <a href="<?php echo $google_auth_url; ?>">Connect with Google</a>
  <?php else : ?>
  <a href="<?php echo URL::to('gapi/logout'); ?>">Logout</a>
  <?php endif; ?>

  <?php if (isset($results)) : ?>
  <pre> <?php var_dump($results); ?> </pre>
  <?php endif; ?>
</body>
</html>