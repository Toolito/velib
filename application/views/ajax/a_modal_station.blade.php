		<script type="text/javascript">
		$(document).ready(function(){
			$('.see_modal').click(function(e){
				$(this).each(function(){ 
					e.preventDefault();
					$('#seeStationModal').modal();
					var station_id = $(this).attr('rel');
					//alert(station_id);

					$.get(BASE+'/station/msee/'+station_id, function(data) {
						var numero = data.numero;
						var nom = data.nom;
						var adresse = data.adresse;
						content = '<address>';
		                content = '<strong>Station n°'+numero+'</strong><br>';
		                content += nom+"<br>";
		                content += adresse+"<br>";
		                content += "</address><br><br>";
		                $('.modal-body').empty().append(content);
		                $('.modal-header>h3').empty().append(nom);
			               // updateListing(data[x]);					
						//var adresse = $(data).find('nom').text();
					});
					$.get(BASE+'/station/see/'+station_id, function(data) {
						var available = $(data).find("available").text();
						var free = $(data).find("free").text();
						var total = $(data).find("total").text();
						var ticket = $(data).find("ticket").text();
						var open = $(data).find("open").text();
						var updated = $(data).find("updated").text();
						var connected = $(data).find("connected").text();
						content = '<address">';
		                content = '<strong>Informations sur cette station</strong><br>';
		                content += "Nombre de vélos : "+free+"<br>";
		                content += "Nombre d\'emplacements libres : "+available+"<br>";
		                content += "Nombre total d\'emplacements : "+total+"<br>";
		                content += "Nombre de tickets : "+ticket+"<br>";
		                content += "Ouvert : "+open+"<br>";
		                content += "Dernière mise à jour : "+updated+"<br>";
		                content += "Connectés : "+connected+"<br>";
		                content += "</address>";
		                $('.modal-body').append(content);					
						//alert(data);
						//var test = $(data).children("available").text();
						//$('#content').append(test);
					});				

					/*$.ajax({
						type: "GET",
						url: "/station/see/",
						data: ""
						dataType: "html",
						success: function(xmlData)
						{
							$('.modal-body').empty().append(station_id);
							$(xmlData).find("station").each(function() {
								
								var station = $(this); 
								var velosDispos = station.children("available").text();
								var emplacementsDispos = station.children("free").text();
								var total = station.children("total").text();
								var html = '<form class="from-horizontal">';
								html += '<div class="control-group"><label class="control-label">Numéro</label><div class="controls">'+total+'</div></div>';
								html += '<div class="control-group"><label class="control-label">Nom</label><div class="controls">'+emplacementsDispos+'</div></div>';
								html += '<div class="control-group"><label class="control-label">Adresse</label><div class="controls">'+velosDispos+'</div></div>';		
								html += '</form>';
								alert(html);
								$('#seeStationModal.modal-body').empty();
							});
						}
					});		*/				
				});
			});
		});
		</script>

		<div class="modal hide fade" id="seeStationModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3></h3>
			</div>
			<div class="modal-body">

			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-dismiss="modal">Fermer</a>
			</div>
		</div>