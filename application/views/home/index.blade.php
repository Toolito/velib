@layout('main')
@section('begin_script')
<script src="/js/jquery-1.7.2.min.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="UTF-8" src="https://maps.googleapis.com/maps/api/js?sensor=true&libraries=geometry"></script>
<script type="text/javascript"  charset="UTF-8" src="http://geoxml3.googlecode.com/svn/branches/polys/geoxml3.js"></script>
<script type="text/javascript" charset="UTF-8" src="http://google-maps-utility-library-v3.googlecode.com/svn/trunk/infobubble/src/infobubble.js"></script>
<script type="text/javascript" charset="UTF-8" src="/js/carte-velib-ajax.js"></script>
@if(!$user->hasStations())
<script>
    /*function LatLong(degLat, degLong) {
         this.lat = LatLong.llToRad(degLat);
         this.lon = LatLong.llToRad(degLong);
       }    
       
       LatLong.llToRad = function(brng) {
         if (!isNaN(brng)) return brng * Math.PI / 180; 
       
         brng = brng.replace(/[\s]*$/,'');               
         var dir = brng.slice(-1).toUpperCase();         
         if (!/[NSEW]/.test(dir)) return NaN;           
         brng = brng.slice(0,-1);                       
         var dms = brng.split(/[\s:,Â°Âºâ€²\'â€³\"]/);         
         switch (dms.length) {                           
           case 3:                                       
             var deg = dms[0]/1 + dms[1]/60 + dms[2]/3600; break;
           case 2:                                       
             var deg = dms[0]/1 + dms[1]/60; break;
           case 1:                                       
             if (/[NS]/.test(dir)) brng = '0' + brng;   
             var deg = brng.slice(0,3)/1 + brng.slice(3,5)/60 + brng.slice(5)/3600; break;
           default: return NaN;
         }
         if (/[WS]/.test(dir)) deg = -deg;               
         return deg * Math.PI / 180;                     
       }
       
       LatLong.distHaversine = function(p1, p2) {
         var R = 6371;
         var dLat  = p2.lat - p1.lat;
         var dLong = p2.lon - p1.lon;

         var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                 Math.cos(p1.lat) * Math.cos(p2.lat) * Math.sin(dLong/2) * Math.sin(dLong/2);
         var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
         var d = R * c;
       
         return d;
       }
startPoint = new LatLong(lat, lng);
endPoint = new LatLong(lat, lng);
var dist = LatLong.distHaversine(startPoint, endPoint);*/ // exprimé en Km.
  
var centreCarte = new google.maps.LatLng(48.857148, 2.351031);</script>

@else
<script>var centreCarte = new google.maps.LatLng(48.857148, 2.351031);</script>
@endif
<script>
$(document).ready(function(){
	var mapOptions = {
		zoom: 14,
		center: centreCarte,
		panControl: false,
		zoomControl : false,
		scaleControl: false,
		streetViewControl: false,
		mapTypeId : google.maps.MapTypeId.ROADMAP
    }

	var map = new google.maps.Map(document.getElementById(mapId),mapOptions);
	chargeCarteVelib(map); // on charge la carte velib
	
	//calcNearestStation();

	//GEOLOCALISATION

	
	/*function successCallback(position)
	{
		map.panTo(new google.maps.LatLng(position.coords.latitude, position.coord.longitude));
		var marker = new google.maps.Marker({
			position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
			map: map
		});
		if(previousPosition){
			var newLineCoordinates = [
				new google.maps.LatLng(previousPosition.coords.latitude, previousPosition.coords.longitude),
				new google.maps.LatLng(position.coords.latitude, position.coords.longitude)];
			
			var newLine = new google.maps.Polyline({
				path: newLineCoordinates,
				strokeColor: "#FF0000",
				strokeOpacity: 1.0,
				strokeWeight: 2
			});
			newLine.setMap(map);
		}
		previousPosition= position;
		
	};*/
	
	//DIRECTIONS
	/*var directionsRenderer = new google.maps.DirectionRenderer();
	directionsRenderer.setMap(map);
	directionsRenderer.setPanel(document.getElementById('routes-map'));
	var directionService = new google.maps.DirectionsService();
	var request = {
		origin: "Sydney, NSW",
		destination: "Chatswood, NSW",
		travelMode: google.maps.DirectionsTravelMode.CYCLING,
		unitSystem: google.maps.DirectionsUnitSystem.METRIC,
		provideTripAlternatives: true
	};
	directionService.route(request, function(response,status) {
		if(status == google.maps.DirectionsStatus.OK) {
			directionsRenderer.setDirections(response);
		}
		else{
			alert('Error: '+status);	
		
		}
	});*/
	//ajoutInfos(map); // on ajoute les fonctionnalit�s suppl�mentaires
	
});
</script>

@endsection
@section('content')

<?=View::make('breadcrumbs')?>
<div class="row-fluid sortable">
	@if($user->hasStations())
	<div class="box span7">
		<div class="box-header">
			<h2><i class="icon-hand-up"></i><span class="break"></span>Carte des stations</h2>
		</div>			
		<div class="box-content">
			<div id="map" style="height: 500px;"></div>
		</div>
	</div>	
	<div class="box span5">
		<div class="box-header" data-original-title>
			<h2><i class="icon-user"></i><span class="break"></span>Mes stations </h2><span style="margin-left: 10px" class="label label-reverse"><?=count($user->stations()->get())?></span>
			<div class="box-icon">
				<a href="#" class="btn-minimize"><i class="icon-chevron-up"></i></a>
				<a href="#" class="btn-close"><i class="icon-remove"></i></a>
			</div>
		</div>
		<div class="box-content">
			<table class="table table-striped min-datatable" id="table_fav">
			  <thead>
				<tr>
					<th>Numero</th>
					<th>Nom</th>
					<th>Adresse</th>
					<th>Actions</th>
				</tr>									
			  </thead>
			  <tbody>
					@foreach($fav_stations as $f)
					<tr>
						<td>{{$f->numero}}</td>
						<td>{{$f->nom}}</td>
						<td>{{$f->adresse}}</td>
						<td class="center">
							<a class="label label-success see_modal" rel="<?=$f->numero?>" href="#">
								<i class="icon-zoom-in icon-white"></i>  
							</a>
							<a class="label label-important" href="/user/del_favorite/<?=$f->id?>">
								<i class="icon-trash icon-white"></i> 
							</a>
						</td>
					</tr>
					@endforeach
			  </tbody>
			</table>
		</div>	
	</div>
	@else
	<div class="box span12">
		<div class="box-header">
			<h2><i class="icon-hand-up"></i><span class="break"></span>Carte des stations</h2>
		</div>			
		<div class="box-content">
			<div id="map" style="height: 500px;"></div>
		</div>
	</div>
	@endif
</div>
<div class="row-fluid sortable">
	
	<div class="box span12">
		<div class="box-header">
			<h2><i class="icon-th"></i> Itineraire vers une station favorite </h2>
		</div>
		<div class="box-content">
			<div id="route-maps" style="width:100%;height: 100%;"></div>
		</div>
	</div><!--/span-->

</div><!--/row-->			
	
	@section('scripts')
<script type="text/javascript">var BASE = "<?php echo URL::base(); ?>";</script>
		<?=Messages::get_noty();?>

		<?= View::make('ajax.a_modal_station');?>


		<script type="text/javascript">
		$(document).ready(function(){		
			$('.add_station').click(function(e){
				$(this).each(function(){
					e.preventDefault();
					alert('caca');
				});
			});
			
				//$('#carte').removeClass('offset6');
				//$('#roles_display').css("display","block");	
				//$('#roles_html').append("<p>"+ve+"</p>");
				/*$.ajax({
					type: 'GET',
					url: '/villes/test',
					success: function(data){
						$('#carte').removeClass('offset6');
						$('#roles_display').css("display","block");	
						$('#roles_html').append("<p>"+data+"</p>");	

					}
				});	*/
		});
		</script>


	@endsection
	
@endsection
