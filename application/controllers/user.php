<?php

class User_Controller extends Base_Controller {
   public $restful = true;

   public function get_add_favorite($station_number = false){
		if(!$station_number) return Redirect::to('');
		$station= Station::where('numero','=',$station_number)->first();
		if(!$station) return Redirect::to('');
      $user = Auth::user();
      if($user->hasStationById($station_number)){
		$user->stations()->attach($station->id);
		$this->data['fav_stations']= $user->stations()->get();
		Messages::add_noty('information',"Ajout de la station $station->nom aux favoris",'bottom');      	
      }
      else{
		Messages::add_noty('error',"az",'bottom');      	      	
      }

		return Redirect::to('');
	}
	
	public function get_del_favorite($station_id = false){
		if(!$station_id) return Redirect::to('');
		$station=Station::find($station_id);
		if(!$station) return Redirect::to('');
		$user=Auth::user();
		$user->stations()->detach($station_id);
		Messages::add_noty('information',"Station $station->nom retire des favoris",'bottom');
		return Redirect::to('');   		
	}
	
	public function post_login(){
	    $userdata=array(
		    'username'=>Input::get('username'),
		    'password'=>Input::get('password')
		    );

	    if(Auth::attempt($userdata)) //Le login est bon, la session est cr�� automatiquement.
	    {
		    Messages::add_noty('success','Bienvenue '.Auth::user()->username.' !','topCenter');
		    Section::inject('title','Accueil');
		    return Redirect::to('');
	    }else{
		    Messages::add_noty('error','Identifiants incorrects !','top');
		    return Redirect::to('login');
	    }
	Section::inject('title','Login page');
	return View::make('login',$this->data);	    
	}
	
	public function post_register()
	{
		if(Auth::check()){return Redirect::to('/');}

			$rules=array(
				'password'=>'confirmed'
				);
			$validation = Validator::make(Input::all(), $rules);

			if ($validation->fails())
			{
				return Redirect::to('register')->with_errors($validation);
			}
			else
			{
				$usercreate=array(
					'username'=>Input::get('username'),
					'password'=>Input::get('password')
					);

				User::create($usercreate);
				Messages::add_noty('success','Compte cree avec succes !','top');
				return Redirect::to('login');				
			}
		
	}
	
	public function get_social_login()
	{
		$user_data = Session::get('oneauth');
		$user = User::where_social_provider($user_data['provider'])
					->where_social_uid($user_data['info']['uid'])
					->first();
					
		if(!is_null($user))
		{
			Auth::login($user->id,true);
			return Redirect::to('/');
		}
		else{
			Message::add_noty('error','aze','top');
			return Redirect::to('login');
		}
		
		Session::forget('user_data');
		return Redirect::to('user/login');
	}
	
	public function get_social_register()
	{
		$user_data = Session::get('oneauth');
		$user= new User;
		//Used for logging in user
		$user->social_uid = $user_data['info']['uid'];
		$user->social_provider = $user_data['provider'];
		
		//General info

		
		switch($user_data['provider'])
		{
			case 'facebook' :
				$user->username = $user_data['info']['nickname'];
				$user->email = $user_data['info']['email'];				
				$email_check = User::where_email($user_data['info']['email'])
									->count();
				if($email_check ==0)
				{
					$user->email == $user_data['info']['email'];
				}
			break;
			case 'twitter' :
				$user->username = $user_data['info']['name'];			
			break;
			case 'google' :
				$user->username = $user_data['info']['nickname'];
				$user->email = $user_data['info']['email'];
			break;
		}
		
		$user->save();
		Auth::login($user->id,true);
		
		Session::forget('user_data');
		Messages::add_noty('success','Bienvenue sur VeliBike !','topCenter');
		return Redirect::to('/');
	}
	
	
}