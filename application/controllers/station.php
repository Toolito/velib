<?php 

class Station_Controller extends Base_Controller {
   public $restful = true;

   public function get_msee($station_id){
   	if(!$station_id) return Redirect::to('');
   	$station = Station::where('numero','=',$station_id)->first();
		return Response::eloquent($station);
		//Renvoi $contents, $nom_station, $numero_station, $adresse_station
   }

   public function get_see($station_id){
		$urldetailStation = "http://www.velib.paris.fr/service/stationdetails/".$station_id;
		header("content-type: text/xml");
		$handle = fopen($urldetailStation, "rb");
		$contents = '';
		while (!feof($handle)) {
			$contents .= fread($handle, 8200);
		}
		fclose($handle);

		//$contents = Formatter::make($contents, 'xml')->to_json();

		return Response::json($contents);
   }

}

 ?>