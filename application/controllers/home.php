<?php

class Home_Controller extends Base_Controller {
	public function action_index()
	{
		if(Auth::guest()){return Redirect::to('login');}
		$user = Auth::user();
		$this->data['user']=Auth::user();
		$this->data['fav_stations']= $user->stations()->get();
		Section::inject('title','Accueil'); //Injecte 'Accueil' dans le yield('title') qui sera dans la view
		return View::make('home.index',$this->data); //Affiche la view home/index.blade.php
	}
	
	public function action_timeline()
	{
		return View::make('timeline');
	}

	public function action_login()
	{	
		if(Request::method()=='POST')
		{
			$userdata=array(
				'username'=>Input::get('username'),
				'password'=>Input::get('password')
				);

			if(Auth::attempt($userdata)) //Le login est bon, la session est créé automatiquement.
			{
				Messages::add_noty('success','Bienvenue '.Auth::user()->username.' !','topCenter');
				return Redirect::to('');
			}
			else
			{
				Messages::add_noty('error','Identifiants incorrects !','top');
				return Redirect::to('login');
			}
		}
		Section::inject('title','Login page');
		return View::make('login',$this->data);
	}

	public function action_register()
	{
		if(Auth::check()){return Redirect::to('/');}

		if(Request::method()=='POST')
		{
			$rules=array(
				'password'=>'confirmed'
				);
			$validation = Validator::make(Input::all(), $rules);

			if ($validation->fails())
			{
				return Redirect::to('register')->with_errors($validation);
			}
			else
			{
				$usercreate=array(
					'username'=>Input::get('username'),
					'password'=>Input::get('password')
					);

				User::create($usercreate);
				Messages::add_noty('success','Compte créé avec succès !','top');
				return Redirect::to('login');				
			}
		}
		return View::make('register');
	}

	public function action_logout()
	{
		
		if(Session::has('token'))
		{
			Session::forget('token');
			Auth::logout();
		}
		Auth::logout();
		Messages::add_noty('success','Vous êtes déconnecté !','top');
		return Redirect::to('login');
	}
	


}
