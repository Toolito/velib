<?php

class Base_Controller extends Controller {

	//Data pour les controllers
	public $data = array();

	public function __construct()
	{
		parent::__construct();
		$this->data['ajax']=false;
		$this->data['user']=Auth::user();
	}

	/**
	 * Catch-all method for requests that can't be matched.
	 *
	 * @param  string    $method
	 * @param  array     $parameters
	 * @return Response
	 */
	public function __call($method, $parameters)
	{
		return Response::error('404');
	}

}