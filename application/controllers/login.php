<?php

class Login_Controller extends Base_Controller {
    public function action_oauth()
	{
      $facebook= IoC::resolve('facebook-sdk');
		if(Input::get('error')){
			return Redirect::to('');
		}
		$uid = $facebook->getUser();
		$fbuser = $facebook->api('/me');
		$user = User::where('oauth_uid','=', $uid)->or_where('email','=',$fbuser['email'])->first();
		if(is_null($user)){
			Message::add_noty('error','Facebook login failed','top');
			return Redirect::to('');
		} else{
			Auth::login($user);
			return Redirect::to('/');
		}
	}
    
}