<?php
class User extends Eloquent{
	//public static $table='users';
	//public static $hidden = array('nickname');
	public static $timestamps = true;

	public function set_password($password){
		$this->set_attribute('password',Hash::make($password));
	}
	
	public function stations(){
		return $this->has_many_and_belongs_to('Station');
	}
	
	public function hasStations(){
		if(count($this->stations()->get()) ==0){
			return false;
		}else{
			return true;
		}
	}

	public function hasStationById($id){
		if(count($this->stations()->where('numero','=',$id)) > 0)
		{
			return true;
		}
		else{
			return false;
		}

	}


}