<?php

class Create_Stations {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
	Schema::table('stations', function($table)
		{
			$table->create();
			$table->increments('id');
			$table->integer('numero');
			$table->string('nom');
			$table->integer('velos');
			$table->integer('slots');
			$table->string('adresse');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function stations()
	{
		Schema::drop('stations');
	}
}
