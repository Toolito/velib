<?php

class Create_Users {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
	Schema::table('users', function($table)
		{
			$table->create();
			$table->increments('id');
			$table->string('nom');
			$table->string('prenom');
			$table->integer('cpostal');
			$table->string('ville');
			$table->string('adresse');
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
