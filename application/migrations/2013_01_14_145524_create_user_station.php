<?php

class Create_User_Station {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{
	Schema::table('user_station', function($table)
		{
			$table->create();
			$table->increments('id');
			$table->integer('user_id');
         $table->integer('station_id');
         $table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('user_station');
	}

}