/*
	 CARTE VELIB par Olivier Gorzalka > http://www.weblogr.fr
	 Ce script est sous licence Creative Common.
*/

/* VARIABLES */

// positionne le focus de la carte
//var centreCarte = new google.maps.LatLng(48.857148, 2.351031);
// Personnalisation de l'icone du velib 
/*var icon = new google.maps.MarkerImage();
icon.image = "/img/velib.png";
icon.iconSize = new google.maps.Size(12, 20);
icon.shadowSize = new google.maps.Size(0, 0);
icon.iconAnchor = new google.maps.Point(6, 20);
icon.infoWindowAnchor = new google.maps.Point(5, 1);*/

var icon = new google.maps.MarkerImage("/img/velib.png",null,null,null,new google.maps.Size(12,20));

// identififiant de la div contenant la carte
var mapId = "map";
var infoWindow=null;
//var tabLatLng = new array();
/* KML */
// KML contenant les parcours vélib
//VIDE ; var kml = "http://maps.google.fr/maps/ms?ie=UTF8&hl=fr&mpnum=3&msa=0&output=nl&msid=100906356033248349925.000436272d1b3fa08118c"
//var pistes = new GeoXml(kml);
// KML externe contenant les arrondissements
//var kml2 = "http://maps.google.fr/maps/ms?ie=UTF8&hl=fr&msa=0&output=nl&msid=103763259662194171141.000001119b4b856600854"
//var arrondissements = new GeoXml(kml2);

function afficheDataStation(station_number) {
	$.ajax({
		type: "GET",
		url: "/xml/stations-velib.php?action=getInfos&station_number=" + station_number,
		dataType: "xml",
		success: function(xmlData)
		{
			$(xmlData).find("station").each(function() { 
				var marker = $(this); 
				var velosDispos = marker.children("available").text();
				var emplacementsDispos = marker.children("free").text();
				var total = marker.children("total").text();
				var html = "<p><strong>velos disponibles</strong> : "+velosDispos+"</p>";
				html += "<p><strong>emplacements libres</strong> : "+emplacementsDispos+"</p>";
				html += "<p><strong>total</strong> : "+total+"</p>";
				$("#infosStations").replaceWith(html);
			});
		}
	});
}

// fonction permettant de créer les différents icone velib sur la carte
/* poramètres :
	station_number = numéro de la station velib
	point = propriétés du marqueur velib
	html = contenu de la bulle d'infos
*/


/*function returnInfo(station_number, point, html) {
	var marker = makeMarker(point);

	google.maps.event.addListener(marker, 'click', function() {
		infoWindow = new google.maps.InfoWindow({
				content: html + '<div style=\"width:222px;height:30px;display:block\" id=\"infosStations\" /><p class="loader"><img src=\"img/load.gif\" alt="chargement en cours..." /></p></div></div>',
				});	
		//afficheDataStation(station_number);
		infoWindow.open(map,marker);
	
	});

	return marker;
}*/

function makeMarker(point)
{
	var marker=new google.maps.Marker({
		position: point,
		icon:icon,
		clickable: true
	});
	return marker;
}

// fonction permettant de charger la carte avec l'ensemble des velibs
// paramètre : map = identifiant de la carte Google Map
function chargeCarteVelib(map) {
	$.ajax({
		type: "GET",
		url: "/xml/stations-velib.php",
		dataType: "xml",
		success: function(xmlData)
		{
			$(xmlData).find("marker").each(function(m) { 
				var marker = $(this); 
				var lat = marker.attr("lat"); // latitude
				var lng = marker.attr("lng"); // longitude
				//tabLatLng += lat+";"+lng;	
				var address = marker.attr("fullAddress"); // adresse complète
				var label = marker.attr("name"); // nom de la station
				var station_number = marker.attr("number"); // numéro de la station vélib
				var point = new google.maps.LatLng(lat,lng); // on créé les différents points correspondant à des stations vélib
				var html = '<div class="ohidden" style="display:block;"><p><strong>'+label+"</strong></p>";
				html += "<p>"+address+"</p>";
				//var marker2 = returnInfo(station_number,point,html);
				var marker2 = makeMarker(point);
				
							
				google.maps.event.addListener(marker2, 'click', function() {
					infoWindow = new google.maps.InfoWindow({
							content: html + '<div style=\"margin-top:10px;overflow:hidden;display:block\" id=\"infosStations\" /><p class="loader"><img src=\"img/load.gif\" alt="chargement en cours..." /></p></div><a href="/user/add_favorite/'+station_number+'"><span class="label label-success">Ajouter a mes stations préférées</span></div></div>',
							});	
					afficheDataStation(station_number);
					infoWindow.open(map,marker2);
					$('.ohidden').parent().css('overflow', 'hidden');
					infoWindow.setOptions({maxWidth:500})
				});
				//if user has not favorites
				marker2.setMap(map);
				//else
				//calcNearestStation(tabLatLng);


			});
		}
	});
	/*directionsDisplay = new google.maps.DirectionsRenderer();
	directionsDisplay.setMap(map);
	directionsDisplay.setPanel(document.getElementById("route-maps"));
	adresse = document.getElementById("address").value;
	
	var requeteItineraire = {
		origin:adresse,
		destination:fgfg,
		region: "fr",
		travelMode: google.maps.DirectionsTravelMode.BICYCLING
		};*/
}

function getCurrentPosition()
{
	var myCoords = new Array();
	if (navigator.geolocation){
			navigator.geolocation.getCurrentPosition(function(position) {
			
			myCoords[0] = position.coords.latitude;
			myCoords[1] = position.coords.longitude;
			var infopos = "Position déterminée :\n";
			infopos += "Latitude : "+position.coords.latitude +"\n";
			infopos += "Longitude : "+position.coords.longitude+"\n";
			document.getElementById("route-maps").innerHTML = infopos;

		});		
		//var watchId = navigator.geolocation.watchPosition(successCallback, null);
	}else{
		alert("Votre navigateur ne prend pas en compte la geolocalisation HTML5");
	}
	return myCoords;
}

/*function calcNearestStation(tabLatLng)
{
	for(var i=0;i<tabLatLng.length;i++)
	{
		document.getElementById("route-maps").append = tabLatLng;
	}
}*/

function erreurPosition(error) {
    var info = "Erreur lors de la géolocalisation : ";
    switch(error.code) {
    case error.TIMEOUT:
    	info += "Timeout !";
    break;
    case error.PERMISSION_DENIED:
    info += "Vous n’avez pas donné la permission";
    break;
    case error.POSITION_UNAVAILABLE:
    	info += "La position n’a pu être déterminée";
    break;
    case error.UNKNOWN_ERROR:
    	info += "Erreur inconnue";
    break;
    }
document.getElementById("infoposition").innerHTML = info;
}

// fonction permettant d'ajouter des fonctions supplémentaires à la carte
// paramètre : map = identifiant de la carte Google Map
function ajoutInfos (map) {
	// code html qui sera inséré après la carte pour faire apparaitre les fonctionnalités
	var htmlFunction = '<form action="#" method="post" name="fonctioncarte" id="fonction-carte">';
	htmlFunction += '<p><input type="checkbox" id="arrondissement" name="aff_arrondissement" value="" /><label class="options-carte" for="arrondissement">Afficher les arrondissements</label></p>';
	htmlFunction += '<p><input type="checkbox" id="affichevelib" name="aff_velib" value="" /><label class="options-carte" for="affichevelib">Afficher les pistes cyclables</label></p>';
	htmlFunction += '</form>';
	// on ajoute le code html à la suite de la carte
	$("#map").after(htmlFunction);

	//affiche les parcours velib
	$('#affichevelib').click(function() {
		if ($(this).attr("checked") == true) // verifie si le champs est coché
			pistes.setMap(map);
		else
			map.removeOverlay(pistes);
	});
	
	//affiche les arrondissements
	$('#arrondissement').click(function() {
		if($(this).attr("checked")  == true) // verifie si le champs est coché
			map.addOverlay(arrondissements);
		else
			map.removeOverlay(arrondissements);
	});
	
}

